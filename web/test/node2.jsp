<%-- 
    Document   : node2
    Created on : Oct 23, 2015, 4:46:25 PM
    Author     : trossky
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:h="http://xmlns.jcp.org/jsf/html">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='http://fonts.googleapis.com/css?family=Oxygen|Lato:300|Open+Sans:300' rel='stylesheet' type='text/css'></link>
        <title>Node 2</title>
        
        <link href="../resources/index/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../resources/index/style.css" rel="stylesheet"/>
        <link href="../resources/index/css/animate-custom.css" rel="stylesheet"/>
    <script type="text/javascript" src="../resources/index/js/jquery.js"></script>
    <script src="../resources/index/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../resources/index/js/jmpress.min.js"></script>
        <script type="text/javascript" src="../resources/index/js/jquery.jmslideshow.js"></script>
        <script type="text/javascript" src="../resources/index/js/jquery.bxslider.min.js"></script>
    <script src="../resources/index/js/modernizr.custom.js"></script>
    <script src="../resources/index/js/jquery.dlmenu.js"></script>
    </head>
    <body>
       <div class="container">
    	<div class="header pull-left">
            <a href="/servletIndex" class="logo"><img src="../resources/index/img/logo.png" alt="logo" /></a>
            <nav class="navbar navbar-default pull-right" role="navigation">
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a href="/servletIndex">Home</a></li>
                    <li class="active"><a href="test/node2.jsp"> Session ID</a></li>
                    <li><a href="/servletHomeList">Portfolio</a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">User <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                            <li><a href="/servletAuthentication"> Authentication</a></li>
                            <li><a href="/servletRegister">Register</a></li> 
                    </ul>
                  </li>
                  <li><a href="#">Our Team</a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </nav>
            <div id="dl-menu" class="dl-menuwrapper pull-right">
                <button class="dl-trigger">Open Menu</button>
                    <ul class="dl-menu">
                            <li><a href="/servletIndex">Home</a></li>
                            <li class="active"> <a href="test/node2.jsp"> Session ID</a></li>
                            <li><a href="/servletHomeList">Portfolio</a></li>
                            <li><a href="#">User</a>
                                    <ul class="dl-submenu">
                                        <li><a href="/servletAuthentication"> Authentication</a></li>
                                         <li><a href="/servletRegister">Register</a></li> 
                                    </ul>
                            </li>
                            <li><a href="#">Our Team</a></li>
                    </ul>
            </div><!-- /dl-menuwrapper -->
        </div>
    </div>
    <div class="slider-container pull-left text-center">
    	<section id="jms-slideshow" class="jms-slideshow">
            <div class="step" data-color="color-1" data-x="2000" data-y="1000" data-z="3000" data-rotate="-20">
                
                <div class="jms-content">
                    <h3>Session ID: <%=request.getSession().getId()%></h3> 
                        <% Long counter = (Long) request.getSession().getAttribute("counter");
                            if (counter == null) {
                                    counter = 0l;
                                }
                            counter++;
                            request.getSession().setAttribute("counter", counter);%> 
                    <h3>current counter value is:  ${counter}</h3> 
                </div>
                                    
            </div>
	</section>    
    </div>
        
        <script type="text/javascript">
			$(function() {
				
				var jmpressOpts	= {
					animation		: { transitionDuration : '0.8s' }
				};
				
				
				$( '#jms-slideshow' ).jmslideshow( $.extend( true, { jmpressOpts : jmpressOpts }, {
					autoplay	: true,
					bgColorSpeed: '0.8s',
					arrows		: false
				}));
				
				
				$('.dropdown').hover(function(){
			 		$(this).find('.dropdown-menu').addClass('animated bounceIn');
				},
				function(){
					$(this).find('.dropdown-menu').removeClass('animated bounceIn');
				});
				
			
				//preloader and home animation starter
				$(window).load(function(){
					//$('#pre_loader_mask').hide().remove();
					//start home animation
					
					$('.services').hover(function(){
					$('.services-box-2').addClass('animated fadeInRight');
					}); 
					
					$('.services').hover(function(){
					$(this).find('.services-box-1').addClass('animated fadeInLeft');
					});
					
					$('.process').hover(function(){
					$(this).find('.process-box').addClass('animated bounceIn');
					});
					
					$('.subscribe').hover(function(){
					$(this).find('.input-lg').addClass('animated rotateInDownLeft');
					$(this).find('.btn-effect').addClass('animated rotateIn');
					});
					
					//collapse circular menu after 7 seceond
					setTimeout(circularMenuConroll,7000);
				});


			$('.bxslider').bxSlider({
			  minSlides: 6,
			  maxSlides: 2,
			  slideWidth: 800,
			  slideMargin: 10
			});
			
			
				
			$( '#dl-menu' ).dlmenu();
				
				
			});
	</script>   
    </body>
</html>
