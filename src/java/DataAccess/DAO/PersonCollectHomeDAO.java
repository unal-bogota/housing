/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataAccess.DAO;

import DataAccess.Entity.Person;
import DataAccess.Entity.Personcollecthome;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author trossky
 */
public class PersonCollectHomeDAO {
    
    public  Personcollecthome persist( Personcollecthome personcollecthome){
        
        EntityManager em;
        EntityManagerFactory emf;
        
        emf=Persistence.createEntityManagerFactory("housingPU");
        em=emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            em.persist(personcollecthome);
            em.getTransaction().commit();
            
        } catch (Exception e) {
            em.getTransaction().rollback();
        }finally{
            em.close();
            return personcollecthome;
        }
        
    }
    
    public Personcollecthome favorite(String collectionDescription,
            Long idcollection,long idperson,String hometype,long idhome) {
        
        EntityManagerFactory emf= Persistence.createEntityManagerFactory("housingPU");
        EntityManager em= emf.createEntityManager();
        Personcollecthome p=null; 
        
        
        String consult="insert into Personcollecthome values("+collectionDescription+","+idcollection+","+idperson+","+hometype+","+idhome+");";
        
        //consult="SELECT p FROM Person p WHERE p.user = "+user;
        
        Query q= em.createQuery(consult);
        
        
        try {
            p=(Personcollecthome) q.getSingleResult();
            
            System.out.println(q.getSingleResult());
        } catch (Exception e) {
        }finally{
            em.close();
            return  p;
        }
        
        
    }
    
    
    
    
}
