/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation.Servlet;

import BusinessLogic.Controller.HomeController;
import BusinessLogic.Controller.ManageAuthentication;
import DataAccess.Entity.Person;
import Presentation.Bean.AuthenticationBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author trossky
 */
public class ServletStartProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServletStartProfile</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletStartProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       response.sendRedirect("faces/profile/user.xhtml");
        if(request.getSession().getAttribute("user")==null){
            request.getRequestDispatcher("faces/person/authentication.xhtml").forward(request, response);
             
            
        }else{
            request.getRequestDispatcher("faces/profile/user.xhtml").forward(request, response);
            
            
        }
        
       
         //request.getRequestDispatcher("faces/profile/user.xhtml").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ManageAuthentication authentication = new ManageAuthentication();
        
        String user= request.getParameter("user");
        String pass= request.getParameter("pass");
        
        if(request.getSession().getAttribute("AttributeRol")==null){
            if (authentication.authentication(user, pass).equals("Welcome")) {
                
                HttpSession httpSession=request.getSession();
                httpSession.setAttribute("AttributeUser",authentication.getUser());
                httpSession.setAttribute("AttributeRol",authentication.getRol() );
                httpSession.setAttribute("AttributeIdPerson",authentication.getIdperson());
                
                    if(authentication.getRol().equals("admin")){
                         request.getRequestDispatcher("faces/profile/admin.xhtml").forward(request, response);
                            
                    }else{request.getRequestDispatcher("faces/profile/user.xhtml").forward(request, response);}
            }else {
                request.getRequestDispatcher("faces/person/authentication.xhtml").forward(request, response);
                
            }
        }else{
            request.getRequestDispatcher("faces/profile/user.xhtml").forward(request, response);
                
            }
      
            
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

