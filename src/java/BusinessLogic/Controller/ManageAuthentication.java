/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessLogic.Controller;

import DataAccess.DAO.PersonDAO;
import DataAccess.DAO.PersonFacade;
import DataAccess.Entity.Person;
import java.math.BigInteger;

/**
 *
 * @author trossky
 */
public class ManageAuthentication {
    private String address;
    
    private BigInteger document;
    private  Long idperson;

    public Long getIdperson() {
        return idperson;
    }

    public void setIdperson(Long idperson) {
        this.idperson = idperson;
    }

   
    
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    private String lastName;
    
    private String name;
    
    private String password;
    
    
    private String phone;
    
    private String profession;
    
    
    private String rol;
    
    private BigInteger salary;
    
    private String user;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigInteger getDocument() {
        return document;
    }

    public void setDocument(BigInteger document) {
        this.document = document;
    }

    

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public BigInteger getSalary() {
        return salary;
    }

    public void setSalary(BigInteger salary) {
        this.salary = salary;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
    
   public  String authentication(String user1, String password){
       
       PersonFacade personDAO = new PersonFacade();
       
       
        Person personE = personDAO.findByUser(user1,password);
        
        
        if (personE != null) {
            
            //Obtengo el id de person para crear la seccion y todo los datos
            address=personE.getAddress();
            document=personE.getDocument();
            email=personE.getEMail();
            idperson=personE.getIdperson();
            lastName=personE.getLastName();
            name=personE.getName();
            phone=personE.getPhone();
            profession=personE.getProfession();
            rol=personE.getRol();
            salary=personE.getSalary();
            user=personE.getUser();
            
             System.out.println(personE.getName());
            return "Welcome";
        } else {
            return "User not found!";
        }

   }
   
   
    
}
